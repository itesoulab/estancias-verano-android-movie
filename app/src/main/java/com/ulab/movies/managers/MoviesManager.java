package com.ulab.movies.managers;

import com.ulab.movies.models.Movie;

import java.util.ArrayList;

public class MoviesManager {

    private ArrayList<Movie> mMovies;

    private static MoviesManager sInstance;

    public static MoviesManager getsInstance(){
        if (sInstance == null) {
            sInstance = new MoviesManager();
        }
        return sInstance;
    }

    private MoviesManager(){
        mMovies = new ArrayList<>();
    }

    public ArrayList<Movie> getMovies(){
        return this.mMovies;
    }

    public void addMovie( Movie movie){
        this.mMovies.add(movie);
    }

    public Movie getMovieByTitle(String Title){
        for (Movie m:
             mMovies) {
            if( m.getTitle().equals(Title))
                return m;
        }

        return null;
    }


}
