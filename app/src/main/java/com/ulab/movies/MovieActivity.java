package com.ulab.movies;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;
import com.ulab.movies.managers.MoviesManager;
import com.ulab.movies.models.Movie;

import java.io.InputStream;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class MovieActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    private MoviesManager mMoviesManager;
    ImageView mImageView;
    TextView mMovieTitle;
    TextView mMovieYear;
    TextView mMovieDescription;
    static final int RECOVERY_REQUEST = 1;
    YouTubePlayerView youTubeView;
    YouTubePlayer mYoutubePlayer;
    MaterialRatingBar ratingBar;
    String title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        Intent intent = getIntent();
        this.title = intent.getStringExtra("movieTitle");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        this.mImageView = (ImageView) this.findViewById(R.id.det_thumbnail);

        this.mMovieTitle = (TextView) this.findViewById(R.id.det_movieTitle);
        this.mMovieYear = (TextView) this.findViewById(R.id.det_movieYear);
        this.mMovieDescription = (TextView) this.findViewById(R.id.det_movieDescription);

        this.ratingBar = (MaterialRatingBar) findViewById(R.id.ratingBar);

        this.youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);

        this.youTubeView.initialize("AIzaSyC4N_qSTxmZPh-EhnPCUTGGQhpdXxiCWPY", this);        this.mMoviesManager = MoviesManager.getsInstance();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onInitializationSuccess(Provider provider, YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
            this.mYoutubePlayer = player;
            Movie foundMovie = mMoviesManager.getMovieByTitle(this.title);

            this.mMovieTitle.setText(foundMovie.getTitle());
            this.mMovieYear.setText(foundMovie.getYear());
            this.mMovieDescription.setText(foundMovie.getDescription());

            this.mYoutubePlayer.loadVideo(foundMovie.getUrlVideo());
            this.mYoutubePlayer.play();

            new DownloadImageTask(this.mImageView)
                    .execute(foundMovie.getUrlThumbnail());

            this.ratingBar.setNumStars(5);
            this.ratingBar.setRating(4.5f);

            this.ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                public void onRatingChanged(RatingBar ratingBar, float rating,
                                            boolean fromUser) {
                    Toast.makeText(getApplicationContext(), String.valueOf(rating), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public void onInitializationFailure(Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error = String.format(getString(R.string.player_error), errorReason.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize("AIzaSyC4N_qSTxmZPh-EhnPCUTGGQhpdXxiCWPY", this);
        }
    }

    protected Provider getYouTubePlayerProvider() {
        return youTubeView;
    }


    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        private DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
