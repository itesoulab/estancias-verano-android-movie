package com.ulab.movies;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.ulab.movies.adapters.MoviesAdapter;
import com.ulab.movies.managers.MoviesManager;
import com.ulab.movies.models.Movie;
import com.ulab.movies.listeners.*;

public class HomeAcitivity extends AppCompatActivity {

     RecyclerView mRecyclerView;
     RecyclerView.Adapter mAdapter;
     RecyclerView.LayoutManager mLayoutManager;
     MoviesManager mMoviesManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_acitivity);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        StaggeredGridLayoutManager mStaggeredLayout = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mStaggeredLayout);

        mMoviesManager = MoviesManager.getsInstance();

        mMoviesManager.addMovie(new Movie("Titanic", "1998", "Movie Description", "LlVwU8mA0I", "https://i.ytimg.com/vi/dRD6SNrouPg/maxresdefault.jpg"));

        mMoviesManager.addMovie(new Movie("Lion King", "1992", "Lion King is a sad story", "GibiNy4d4gc", "http://cdn.collider.com/wp-content/uploads/2016/04/the-lion-king-image.jpg"));

        mAdapter = new MoviesAdapter(mMoviesManager.getMovies());
        mRecyclerView.setAdapter(mAdapter);


        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                mRecyclerView, new RecyclerTouchListener.ClickListener() {

            @Override
            public void onClick(View view, final int position) {
                MoviesAdapter mAdapter = (MoviesAdapter) mRecyclerView.getAdapter();
                Movie movie = mAdapter.getMovie(position);

                Intent intent = new Intent(HomeAcitivity.this, MovieActivity.class);

                intent.putExtra("movieTitle", movie.getTitle());
                startActivity(intent);

            }

            @Override
            public void onLongClick(View view, int position) {
                MoviesAdapter mAdapter = (MoviesAdapter) mRecyclerView.getAdapter();
                Movie movie = mAdapter.getMovie(position);
                Toast.makeText(HomeAcitivity.this, movie.getYear(),Toast.LENGTH_SHORT).show();

            }

        }));
    }

}


