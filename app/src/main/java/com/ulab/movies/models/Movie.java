package com.ulab.movies.models;

public class Movie {
    private String title;
    private String year;
    private String description;
    private String urlVideo;
    private String urlThumbnail;


    public Movie(String _title, String _year, String _description, String _urlVideo, String _urlThumbnail) {
        this.title = _title;
        this.year = _year;
        this.description = _description;
        this.urlVideo = _urlVideo;
        this.urlThumbnail = _urlThumbnail;
    }

    public String getTitle(){
        return this.title;
    }

    public String getYear() {
        return this.year;
    }

    public String getDescription() {
        return this.description;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public String getUrlThumbnail() {
        return urlThumbnail;
    }

}